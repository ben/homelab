# homelab

**my homelab late 2021**

i opted for a central wireguard approach with a hetzner vps and all clients connect to this instance via wireguard.

from here all traffic is proxied with haproxy through the wireguard tunnels.


![Alt text](static/overview.png "overview")


but why, you may ask: I have a need for self hosting a few things like offsite minio backups, gitea and monitoring and so on...

So i needed to connect a handfull locations with NAT and ds-lite together.

the limitations from v4-only for one site and v6 with ds-lite on the other and no static ips,

this seems to be the easiest solution. but i am open to suggestions.

## **advantages**

- dns is configured against a hetzner floating ip, which i can just relocate to another host, or load balance if i want to.
- i can kill the entire vpn, for whatever reason, with a single call to the hetzner api
(i can also reprovision the vps, with ansible, since wireguard can be provisioned with the same keys on different servers)
- security and the certificate is handled on the backends, 
since the vps will only proxy tcp with haproxy through the wireguard tunnels and will not handle tls terminations.
- i can ensure v4 and v6 connectivity for alle kinds of internet uplinks and limitations


## **getting started**


If you want to use this playbook directly please be aware, that it is currently tightly coupled with my setup
there are hardcoded ip's, static files, and other variables, which are currently not decoupled.
look at the TODO section to get a grasp, at what's missing.
So make sure, you do understand what you are doing!

### **vps setup**

- create a hetzner instance and point your dns entries to this instance ip
- optional: create a floating ip, assign your floating ip to the instance and point dns to the floating ip


### **client setup**

new devices or os-reinstall is the same thing.
wireguard has to be configured manually, since i have to be onsite anyway to install hardware or software. 

after (re)-install and initial wireguard connectivity, 

i can use ansible to provision all the other things, like ssh-hardening, docker install, jinja templating and so on, with the wireguard ip's.

the ansible inventory is configured against the wireguard ip's.


### **Ansible**

first copy the example inventory and edit the values according to your needs.
make sure you can reach all your hosts in the inventory, either by the wireguard ip's or via ssh tunnels.
create an ansible-vault with `ansible-vault create inventories/your-inventory/groups_vars/all/vault`
and insert the secrets you need. there is an example vault in the example inventory with plaintext vars, what it would like like if you had created it earlier and copied the values inside.

after that run the playbooks in this order:

1. configure-os # configures the OS and your user 
2. deploy-vps # make sure wireguard server and haproxy are running correctly
3. install-docker # install docker on selected hosts
4. deploy-authelia-proxies # will install traefik and authelia on the proxy nodes
5. optional: deploy-worker #  runs all other containers on worker node
6. optional: deploy-streaming # runs plex and jellyfin on streaming node
7. optional: deploy-authelia-proxies-quick # this only applies changed traefik config, in order to save time

all playbooks are idempotent, so you can run them in whatever order you want, once everything is setup ;)


```
ansible-playbook -i inventories/your-inventory/ configure-os.yml --ask-vault-pw
ansible-playbook -i inventories/your-inventory/ deploy-vps.yml --ask-vault-pw
ansible-playbook -i inventories/your-inventory/ install-docker.yml --ask-vault-pw
ansible-playbook -i inventories/your-inventory/ deploy-authelia-proxy.yml --ask-vault-pw
```

**You have to reload wireguard on the server side if you make changes**

`sudo systemctl restart wg-quick@wg0`

i intentionally did not automate that, for safety reasons, but feel free to do that

## manual work afterwords

there is manual work to do right now which is:
- initial minio bucket creation
- authelia 2fa config for my user
- starting the containers on each host after deployment via ansible


## TODOs

- automate the wireguard server config, based on the inventory vars, how many clients exist, and not solely based on my setup
- automate the traefik dynamic backend conf, based on inventory vars, and not via static files
- automate haproxy config generation with backends, could be backends file, to separate the configs
- update README to explain authelia workflow and secrets
- update README for better backup overview
- make some nice pictures for overall setup :)


#### feedback is greatly appreciated, as well as PR's and issues, if you think something is off or badly configured :)