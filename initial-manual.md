# manual for new devices/os-reinstall 

since there is no wireguard connection, the first time a new device is configured there has to be a manual step. goal is to ensure, ansible can reach the host via wireguard directly or at least through ssh proxies.

## steps to ansible playbook

since all hosts will be debian based, this should be relatively easy

- ensure wireguard is installed
- create wireguard private keys
- update vps role with new client keys and deploy vps again
- copy pub key to client and enable systemd job for wireguard
- restart client and ensure connectivity

from now on, device has a wireguard ip and can be deployed via ansible inventory

if you don't want or can't do that, 
there is always second option to just ssh port forward and ansible to localhost via ssh tunnel


## doing


`sudo -i`

```
apt update && apt install vim wireguard -y
cd /etc/wireguard
touch wg0.conf
wg genkey | tee privatekey | wg pubkey > publickey
```
insert config below into **/etc/wireguard/wg0.conf** with your created private key in line 3 and with the server pub from vps.

```
[Interface]
Address = 10.0.60.<client-wg-ip>/24
PrivateKey = XXX

[Peer]
PublicKey = XXX
AllowedIPs = 10.0.60.0/24
Endpoint = <proxy-ip/fqdn>:51820
PersistentKeepalive = 25
```

`systemctl enable --now wg-quick@wg0`

## server config

at minimum your wireguard server conf will look like this

```
[Interface]
Address = 10.0.60.1/24
PrivateKey = XXX
ListenPort = 51820

# client
[Peer]
PublicKey = <your client key from pubkey file in /etc/wireguard>
AllowedIPs = 10.0.60.<client-wg-ip>/32
PersistentKeepalive = 25
```