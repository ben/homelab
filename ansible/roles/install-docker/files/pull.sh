#!/bin/bash

# find docker-compose files within /opt/compose and execute docker-compose pull for each of them
# cron job will update them every day at 11 AM

echo "pull docker images found in compose files within /opt/compose"

compose_files=$(find /opt/compose -name docker-compose.yml)
echo ${compose_files}
pushd /opt/compose
for compose_file in ${compose_files}
do
sudo docker-compose -f "${compose_file}" pull
done
